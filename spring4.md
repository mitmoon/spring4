## Chapter03 빈라이프 사이클과 빈 범위
----

### 1. 빈 객체의 라이프사이클
* 스프링 컨테이너가 다음 과정을 관리
	* 빈 객체 생성
	* 빈에 프로퍼티 할당
	* 빈의 초기화 수행
	* 빈 사용 후 소멸 처리
* 다음 두 가지 방식을 사용해 라이프사이클을 관리
	* 특정 인터페이스를 상속받아 빈을 구현
	* 스프링 설정에서 특정 메소드를 호출하라고 지정
***

##### 1.1 빈 라이프사이클 개요
* 스프링의 빈 라이프 사이클 개요
	* P127 그림3.1 빈 라이프 사이클 개요
	* 전체 흐름
		* 객체생성/프로퍼티 설정 -> 초기화 -> 사용 -> 소멸
	* 빈의 초기화와 소멸 방법은 각각 세가지가 쌍을 이루어 존재
***

##### 1.2 InitializingBean 인터페이스와 DisposableBean 인터페이스
* 객체 생성과 초기화를 위해 다음 인터페이스를 제공
	* o.s.beans.factory.InitializingBean
		* 빈의 초기화 과정에서 실행될 메서드를 정의
	* o.s.beans.DisposableBean
		* 빈의 소멸 과정에서 실행될 메서드를 정의
* 객체 생성 이외의 추가적인 초기화와 소멸작업이 필요할때 사용
* 대표적인 예는 커넥션 풀의 커넥션의 초기화와 소멸처리
```JAVA
	public class ConnPool implements InitializingBean, DisposableBean {
        @Override
        public void afterPropertiesSet() throws Exception {
        	// 커넥션 풀 초기화 실행 : DB 커넥션을 여는 코드
        }
        
        @Override
        public void destroy() throws Exception {
        	// 커넥션 풀 종료 실행 : open한 DB 커넥션을 닫는 코드
        }
    }
```

* 위 클래스를 빈으로 등록하면 생성 후와 소멸 시점에 해당 기능을 수행한다.
***

##### 1.3 @PostConstruct 애노테이션과 @PreDestroy 애노테이션
* 각각 초기화와 소멸을 담당
* CommonAnnotationBeanPostProcessor 전처리기를 빈으로 등록해줘야 한다.
* &lt;context:annotation-config&gt;를 등록하면 전처리기가 등록 됨
***

##### 1.4  커스텀 init 메서드와 커스텀 destroy 메서드
* 외부에서 제공받은 라이브러리라 라이프사이클에서 지원하는 초기화/소멸 기능을 사용 못할때 사용 가능
```XML
	<bean id="pool3" class="net.madvirus.spring4.chap03.ConnPool3" init-method="init" destroy-method="destroy" />
```
	> XML 설정
	> 초기화와 소멸에 사용할 메소드 이름을 지정하면 된다.

	```JAVA
	@Bean(initMethod = "init", destroyMethod = "destroy")
    public ConnPool3 connPool3(){
    	return new ConnPool3();
    }
	```
	> JAVA 설정
***

##### 1.5 ApplicationContextAware 인터페이스와 BeanNameAware 인터페이스
* o.s.context.ApplicationContextAware
	* 이 인터페이스를 상속받으면 초기화 과정에서 컨테이너를 전달받는다.
* o.s.beans.factory.BeanNameAware
	* 이 인터페이스를 상속받은 빈 객체는 초기화 과정에서 빈 이름을 전달받는다.
***

### 2. 빈 객체 범위 (scope)
* 스프링 빈의 주요 범위
	* 싱글톤 범위
	* 프로토타입 범위
***

##### 2.1 싱글톤 범위
* 빈 객체를 한번만 생성
* 스프링 컨테이너가 초기화되고 종료되기 직전까지 빈 객체는 한 개만 생성 됨
* 표현방식
	* scope="singleton"
	* @Scope("singleton")
***

##### 2.2 프로토타입 범위
* 객체의 원형으로 사용되는 빈
* 프로토타입 범위 빈을 getBean() 등을 이용해 구하면 매번 새로운 객체를 생성한다.
* 표현방식
	* scope="prototype"
	* @Scope("prototype")
***

## Chapter04 Environment, 프로퍼티, 프로필, 메시지
* 환경에 따라 다른 값을 사용할 수 있는 방법
* 다국어 지원의 메시지 기능
***

### 1. Environment 소개
* 스프링 설정을 외부 파일에 두고 사용하는 방법
* Environment가 제공하는 기능
	* 프로퍼티 통합 관리
	* 프로필을 이용해서 선택적으로 설정 정보를 사용할 수 있는 방법 제공
* 시스템환경변수, JVM시스템프로퍼티, 프로퍼티파일 등을 PropertySource로 통합 관리
* 여러 프로필 중에서 특정 프로필을 활성화하는 기능를 제공
***

##### 1.1 Environment 구하기
* ConfigurableApplicationContext의 getEnvironment()를 통해 구할수 있음
***

#### 2. Environment와 PropertySource
* 첫번째 기능은 프로퍼티 값을 제공하는 기능
* 다수의 PropertySource로부터 값을 읽어옴
	* P140 그림4.1
	* 시스템프로퍼티 -> 환경변수 -> 자바 Property -> JNDI 순서
***

##### 2.1 Envorinment에서 프로퍼티 읽기
* Environment를 구한 뒤 읽는 메소드를 사용하면 됨
	```XML
    ConfigurableEnvironment env = context.getEnvironment();
    String javaVersion = env.getProperty("java.version");
    ```
***

##### 2.2 Environment에 새로운 PropertySource 추가하기
* MutablePropertySources의 새 PropertySource 추가 메소드를 사용하면 된다.
* 자바 기반의 @PropertySource 어노테이션을 사용하면 된다.
***

#### 3 Environment를 스프링 빈에서 사용하기
* 스프링에서 Environment 접근 방법
	* o.s.context.EnvironmentAware 인터페이스 구현
		* P145 예제
	* @Autowired 어노테이션을 Environment 필드에 적용
		* P145 예제 - 아래부분
***

#### 4 프로퍼티 파일을 이용한 프로퍼티 설정
* 프로퍼티 파일을 XML과 자바 설정에서 직접 사용하는 방법
***

##### 4.1 XML에서의 프로퍼티 설정 : &lt;context:property-placeholder&gt; 사용
* XML 사용
	```XML
    <beans... >
   
    	<context:property-placeholder location="classpath:/db.properties" />
        
        <bean id="conProvider" .... />
        	<property name="driver" value="${db.driver}" />
            ...
    </beans>
    ```

	> location 속성으로 지정한 파일로부터 정보를 읽어온다.
	> 내부적으로 PropertySourcesPlaceholderConfigurer를 빈으로 등록한다.
	>> location의 프로퍼티 파일을 찾고 없으면 Environment의 프로퍼티를 확인해서 값이 있으면 사용한다.

	> 전체설정에서 동일 태그를 두번 사용하면 태그에서 지정한 파일만 읽어오므로 주의!!!
***

##### 4.2 Configuration 어노테이션을 이용하는 자바 설정에서의 프로퍼티 사용
* PropertySourcesPlaceholderConfigurer와 @Value 어노테이션을 함께 쓴다.
	```JAVA
    @Configuration
    public class ConfigByProp {
    	@Value("${db.driver}")
        private String driver;
        ....
        
        @Bean
        public static PropertySourcesPlaceholderConfigurer() {
        ....
        }
    }
    ```
	> PropertySourcesPlaceholderConfigurer 빈은 static으로 설정한다.

* @PropertySource와 PropertySourcesPlaceholderConfigurer를 함께 사용
	* 자신의 설정을 찾고 없으면 Environment의 프로퍼티 값을 조회한다.
	* P153처럼 사용
***

#### 5 프로필을 이용한 설정
* 환경에 따라 설정 정보를 만들어 각각 별도의 프로필 이름을 부여하고
* 환경에 알맞은 프로필을 선택함으로써, 환경에 따라 다른 설정을 사용할 수 있다.
***

##### 5.1 XML 설정에서 프로필 사용하기
* &lt;beans&gt; 태그의 profile 속성을 사용해서 프로필 이름을 지정한다.
	* P154
	* XML파일이 profile 속성을 가지고 있으면, 해당 프로필을 사용한 경우만 적용된다.
	* 특정 프로필의 사용은 setActiveProfile() 메소드 사용
* beans 태그 중첩과 프로필
	* XML 설정 사용시 하나의 파일에 &lt;beans&gt; 태그를 중첩해서 정의 가능
***

##### 5.2 자바 @Configuration 설정에서 프로필 사용하기
* @Profile 어노테이션을 이용
	* P156 아래 예제
	* 활성화는 setActiveProfiles() 메소드를 사용하거나 시스템 프로퍼티에 값을 설정
* 중첩 @Configuration을 이용한 프로필 설정
	* XML 처럼 중첩 클래스를 이용해서 프로필 설정을 한 곳에 모아서 관리 가능
		* P157 아래 예제
***

##### 5.3 다수 프로필 설정
* 프로필 이름을 두개이상 가질 수 있다.
	* P158 하단
	* 프로필명에 "!"도 사용가능하며 !는 해당 프로필을 사용하지 않는다는 의미
***

#### 6 MessageSource를 이용한 메시지 국제화 처리
* 메시지의 국제화를 지원하기 위해 o.s.context.MessageSource 인터페이스를 제공
* 메소드 정의 - P159 하단
* 등록된 빈 중 이름이 'messageSource'인 MessageSource 타입의 빈 객체를 이용해서 메시지를 가져 온다.
***

##### 6.1 프로퍼티 파일과 MessageSource
* 사용 예
	```JAVA
    String msg = messageSource.getMessage("hello", null, someLocale);
    ```
    > args 파라미터는 플레이스 홀더값을 지정할 때 사용한다.

* 어떤 위치의 프로퍼티 파일을 사용하느냐는 인터페이스 구현 클래스에 따라 다름
***

##### 6.2 ResourceBundleMessageSource를 이용한 설정
* MessageSource 인터페이스의 구현 클래스
* 사용 예
	```XML
    <bean id="messageSource" class="o.s.c.s.ResourceBundleMessageSource">
    	<property name="basenames">
        	<list>
            	<value>message.greeting</value>
                <value>message.error</value>
            </list>
        </property>
    </bean>
    ```
    > message패키지의 greeting과 error 프로퍼티 파일에서 메시지를 읽어온다.
    > 다국어파일 설정은  greeting_en.properties, greeting_ko.properties, 형태로 쓴다.
	> 자바5까지는 메시지프로퍼티 파일을 유니코드를 이용해서 입력해줘야 했다.
	> 자바6부터는 인코딩을 지정해주면 유니코드로 작성하지 않아도 된다.
	>> <property name="defaultEncoding" value="UTF-8" />
***

##### 6.3 ReloadableResourceBundleMessageSource를 이용한 설정
* 클래스뿐만 아니라 특정 디렉토리에 위치한 메시지 파일을 사용할 수 있다.
* 클래스패스를 사용하지 않을 경우, 파일 내용이 변경되었을 때, 변경된 내용이 반영된다.
* basename을 쓸때 클래스패스와 파일 경로 모두 사용 가능하다.
***

##### 6.4 빈 객체에서 메시지 이용하기
* 빈 객체에서 MessageSource 사용 방법
	* ApplicationContextAware인터페이스 구현 후, setApplicationContext()를 호출해서 getMessage() 메소드 사용
	* MessageSourceAware 인터페이스 구현 후, setMessageSource()를 호출해서 getMessage() 메소드 사용
***

## Chapter05 확장포인트와 Property Editor/ConversionService
----
#### 1. 스프링 확장 포인트
* 스프링이 제공하는 기능 확장 방법
	* BeanFactoryPostProcessor를 이용한 빈 설정 정보 변경
		* 빈 객체 생성 전에 적용
	* BeanPostProcessor를 이용한 빈 객체 변경
		* 빈 객체를 생성한 후에 적용
***

##### 1.1 BeanFactoryPostProcessor를 이용한 빈 설정 정보 변경
* 외부프로퍼티를 이용하기 위해 PropertySourcesPlaceholderConfigurer클래스를 사용
	* BeanFactoryPostProcessor 인터페이스를 구현하고 있음
	* 빈 객체를 실제로 생성하기 전에 설정 메타 정보의 변경 용도로 사용
	* 새로운 빈 설정도 추가 가능
* 사용 예
	* 프로퍼티 값을 설정하지 않은 경우 디폴트를 10으로 설정하는 예제
* @Configuration 어노테이션을 이용해서 생성한 빈 객체에는 적용되지 않는다.
	* @Configuration을 이용해서 생성하는 빈 객체는 빈 설정 정보를 만들지 않는다.
***
* BeanDefinition의 주요 메소드
	* 빈 설정 정보를 구하거나 수정할 때 필요한 메서드
	* P174
***

##### 1.2 BeanPostProcessor를 이용한 빈 객체 변경
* 생성된 빈 객체를 변경하는 방법
* 스프링이 빈 객체를 초기화할때 BeanPostProcess의 메소드를 호출함
	* P176 그림5.2 BeanPostProcessor의 호출 시점
* 등록된 빈 중 BeanPostProcessor 인터페이스를 구현한 빈 객체를 BeanPostProcessor로 사용함
	* BeanPostProcessor를 구현했다면 빈으로 등록만하면 됨
* 사용 예제
	*  P177 StockReader 예제
***

#### 2 PropertyEditor와 ConversionService
* 스프링은
	* 문자열의 기본 데이터 타입의 변환을 지원한다
	* 문자열을 Properties 타입이나 URL 등으로 변환할 수 있다.
	* 내부적으로 PropertyEditor를 이용해서 문자열을 알맞은 타입으로 변환한다.
	* 스프링3부터는 ConversionService 를 사용한다.
***

##### 2.1 PropertyEditor를 이용한 타입 변환
* 자바빈 규약
	* 문자열을 특정 타입의 프로퍼티로 변환할 때 PropertyEditor 인터페이스를 사용
	* 자바에서는 editors 패키지에 기본 타입을 위한 PropertyEditor를 제공
	* 스프링은 기본 타입 이외의 추가적인 PropertyEditor를 제공
		* URLEditor 등
***

* 스프링이 제공하는 주요 PropertyEditor
	* P188 표5.2 스프링이 제공하는 주요 PropertyEditor
	* 기본으로 사용되지 않는 PropertyEditor를 XML에서 사용하면 에러 발생함
	* 추가로 PropertyEditor를 등록해줘야 함

* 커스텀 PropertyEditor 구현하기
	* PropertyEditor 인터페이스를 상속 받아 알맞은 메서드를 구현하면 된다.
	* 타입 변환 기능만 필요하다면 PropertyEditorSupport 클래스를 상속받아 setAsText() 메소드를 재정의하면 된다.
		* P190 리스트5.15 String을 Money로 변환하기 위한 PropertyEditor 구현
* PropertyEditor 추가 : 같은 패키지에 PropertyEditor 위치시키기
	* 구현했다면 등록해줘야 한다.
		* 변환 대상 타입과 동일한 패키지에 '타입Editor' 이름으로 PropertyEditor를 구현하면 된다.
			* MoneyEditor 클래스와 같은 이름을 구현하면 됨
* PropertyEditor 추가 : CustomEditorConfigurer 사용하기
	* 이름이 '타입Editor'가 아니라면 CustomEditorConfigurer를 이용해서 설정해줘야 함
		* P191 중하단 부분
		* 타입, 타입PropertyEditor 쌍을 맵으로 받아서 등록해준다.
* PropertyEditor 추가 : PropertyEditorRegistrar 사용하기
	* CustomEditorConfigurer는 PropertyEditor에 매개변수를 설정할 수 없다.
	* PropertyEditorRegistrar를 이용해서 매개변수를 지정한다.
	* 방법
		* PropertyEditorRegistrar 인터페이스를 상속받아 구현
		* CustomEditorConfigurer가 해당 클래스를 사용하도록 설정한다.
***

##### 2.2 ConversionService를 이용한 타입 변환
***


## Chapter12 스프링의 트랜잭션 관리
* 성공적으로 처리되거나 또는 하나라도 실패하면 완전히 실패 처리를 해야하는 경우데 사용

----
#### 1. 트랜잭션이란
* 인터넷 서점의 도서 구매 프로세스
	* 결제 수행
	* 결제 내역 저장
	* 구매 내역 저장
* 여러 과정을 하나의 행위로 묶는 것
* 데이터의 무결성 보장
	* All or nothing
***

##### 1.1 ACID
* 트랜잭션의 네가지 특징
    * Atomicity (원자성)
        * 한 개 이상의 동작을 논리적으로 한 개의 작업 단위로 묶는 것
    * Consistency (일관성)
        * 트랜젹션 수행 후 상태가 기대한 상태가 되는 것
    * Isolation (고립성)
        * 다른 트랜잭션과 독립적으로 실행되어야 한다
        * 동일 자원에 동시 접근은 알맞은 순서로 실행되어야 한다.
    * Durability (지속성)
        * 트랜잭션 완료 후 결과가 지속적으로 유지되어야 한다.
        * 물리적저장소를 통해 트랜잭션 결과를 저장
***

#### 2. 스프링의 트랜잭션 지원
* 코드 기반의 트랜잭션 처리와 선언적 트랜잭션 처리를 지원
* DB 연동 기술에 상관없이 동일한 방식으로 트랜잭션을 처리할 수 있도록 하고 있다.
***

##### 2.1 스프링의 PlatformTransactionManager 설정
* PlatformTransactionManager 인터페이스를 이용해 트랜잭션 처리를 추상화
* DB연동기술에 따라 알맞은 구현 클래스를 제공
	* P507 구현 클래스 관계도
	* 구현 클래스는 관련된 DB 기술에 따라 알맞은 트랜잭션 처리를 수행
* 실제 스프링은 TransactionTemplate 클래스나 선언적 방식을 이용해서 트랜잭션을 처리한다.
* DB 연동 구현 기술에 알맞은 트랜잭션 관리자를 등록해야 한다.
***

##### 2.2 JDBC 기반 트랜잭션 관리자 설정
* JDBC나 MyBatis와 같이 JDBC를 이용해서 데이터베이스 연동을 처리하는 경우
	* DataSourceTransactionManager를 트랜잭션 관리자로 사용
	* P508 하단
* Connection의 commit(), rollback() 등의 메소드를 사용해서 트랜잭션 관리
***

##### 2.3 JPA 트랜잭션 관리자 설정
* JPA를 사용하는 경우
	* JpaTransactionManager를 트랜잭션 관리자로 사용
	* P509 중단 설정파일
* EntityManagerFactory를 이용해서 트랜잭션을 관리
***

##### 2.4 하이버네이트 트랜잭션 관리자 설정
* 하이버네이트를 사용하는 경우
	* HibernateTransactionManager를 트랜잭션 관리자로 사용
	* P509 하단 설정파일
* 전달받은 하이버네이트 Session으로부터 Transaction을 생성한 뒤, Transactiond을 이용해서 트랜잭션을 관리한다.
***

##### 2.5 JTA 트랜잭션 관리자 설정
* 다중 자원에 접근하는 경우 JTA (Java Transaction API)를 이용해서 트랜잭션을 처리한다.
	* P510 중단 설정파일
	* 컨테이너가 제공하는 TransactionManager를 사용하지 않는 경우는 userTransaction 프로퍼티를 이용해서 직접 설정할 수도 있다.
		* P510 하단 설정파일
	* 참고
		* http://springsource.tistory.com/138
***

##### 2.6 트랜잭션 전파와 격리 레벨
* 아래와 같은 트랜잭션 처리와 관련된 부분을 설정으로 지정할 수 있도록 지원
	* 새로운 트랜잭션 생성
	* 기존 트랜잭션을 그대로 사용
	* 기존에 트랜잭션이 잰행중인 상태에서 현재 코드를 실행하는 등
* 스프링 제공 트랜잭션 전파 지원
	* P512 표12.1 트랜잭션 전파와 관련해서 스프링이 지원하는 속성
* 스프링에서 설정 가능한 트랜잭션 격리 레벨
	* P512 표12.2 트랜잭션 격리 레벨
***

#### 3. TransactionTemplate을 이용한 트랜잭션
* 상품구매코드 예제
	* P513
	* 다음 순서로 상품 구매를 처리
		* 상품 정보 조회 - 존재하지 않으면 Exception 발생
		* 결제정보를 DB에 insert
		* 구매 주문정보를 DB에 insert
	* 하나의 트랜잭션에서 처리되어야 한다.
	* 세번째 과정이 실패하며 모든 과정이 롤백되어야 한다.
***

##### 3.1 TransactionTemplate과 TransactionCallback으로 트랜잭션 처리하기
* 직접 트랜잭션을 처리할때 o.s.transaction.support.TransactionTemplate 클래스를 이용한다.
* TransactionTemplate을 사용하려면
	* TransactionTemplate을 빈으로 설정해야 한다.
	* P514 중단 설정파일
		* TransactionTemplate을 설정할 때 transactionManager 프로퍼티에 스프링의 PlatformTransactionManager 빈을 지정해야한다.
	* TransactionTemplate 클래스의 사용 예
		* P514 하단 예제
		* execute() 메소드가 TransactionCallback 타입의 객체를 파라미터로 전달받는다.
		* execute() 메소드는 내부적으로 PlatformTransactionManager를 이용해 트랜잭션을 시작한다.
		* execute() 메소드는 파라미터로 전달받은 TransactionCallback의 doInTransaction() 메소드를 실행한다.
		* P516 그림12.3 TransactionTemplate의 트랜잭션 처리 과정
	* execute() 메소드를 사용할 때는 구현 클래스보다는 임의클래스 또는 람다식을 이용해서 객체를 전달한다.
	* doInTransaction() 메소드 내부에서는 런타임이나 에러 타입의 익셉션만 발생시킬 수 있다.
		* checked 익셉션을 발생시켜야한다면 try-catch블록으로 익셉션 처리 후 롤백 여부를 설정해야 한다.
			* P517 중단 예제
***

##### 3.2 TransactionTemplate의 트랜잭션 설정
* 다음의 트랜잭션 속성을 갖는다.
	* 트랜잭션 전파 속성 : 트랜잭션 필요 (REQUIRED)
	* 트랜잭션 격리 레벨 : 데이터베이스의 기본 (DEFAULT)
	* 트랜잭션 타임아웃 : 없음
	* 읽기 전용 아님
* 다르게 설정하려면 TransactionTemplate 설정시 변경해주면 된다.
	* P518 표12.3 트랜잭션 속성 관련 프로퍼티
	* ex) 기존 트랜잭션 여부 상관없이 새로운 트랜잭션을 시작하고 격리 레벨을 REPEATABLE_READ로 지정하고 싶을때
		* P519 상단 설정
***

#### 4. 트랜잭션과 DataSource
* 앞에서는 JDBC를 위한 템플릿 템플릿 클래스를 이용한 코드를 봤다.
	* ItemDao 구현과 PaymentInfoDao 구현 - P519 하단 예제

* Question
```
	어떻게 각 DAO의 메소드가 한 트랜잭션으로 묶여서 실행될까?
	--> JTA를 사용하지 않는 이상 JDBC의 경우 동일한 Connection을 사용해야 한 트랜잭션으로 묶을수 있는데 말이지.
```
	> 비밀은 JdbcTemplate에 있다.

	```JAVA
		// JdbcTemplate이 내부적으로 Connection을 구할 때 사용하는 코드
	    Connection con = DataSourceUtils.getConnection(getDataSource());
	```
	> 현재코드가 트랜잭션 범위에서 실행되고 있으면, 트랜잭션과 엮여있는 Connection을 리턴한다.
	> 즉 현재 트랜잭션이 진행중일 경우 같은 Connection을 이용한다.

* DataSourceUtils.getConnection() 메소드의 트랜잭션 범위 수행 여부 체크
	* DataSourceTransactionManager가 트랜잭션 시작 시 DataSourceUtils에 알림
	* 즉, DataSource를 직접 사용해서 Connection 객체를 구한다면?

* 스프링의 트랜잭션 지원 기능을 사용하고 싶다면?
	* Connection 객체를 구할 때 DataSourceUtils 클래스를 사용해야 한다.
	* P521 하단 예제
***

#### 5. 선언적 트랜잭션 처리
* 트랜잭션 처리를 설정 파일이나 어노테이션을 이용해서 처리
	* 트랜잭션의 범위, 롤백 규칙 등을 정의한다.
* 정의 방식
	* &lt;tx:advice&gt; 태그를 이용한 트랜잭션 처리
	* @Transactional 어노테이션을 이용한 트랜잭션 설정
***

##### 5.1 tx 네임스페이스를 이용한 트랜잭션 설정
* 트랜잭션 설정 방법
    * tx 네임스페이스를 &lt;beans&gt; 태그에 추가한다.
    * &lt;tx:advice&gt; 태그, &lt;tx:attributes&gt; 태그, &lt;tx:method&gt; 태그를 이용해 트랜잭션 속성 정의
    * P522 하단 설정
    	* &lt;tx:advice&gt; 태그는 트랜잭션을 적용할 때 사용될 Advisor를 생성한다.
    	* id속성은 생성될 트랜잭션 Advisor의 식별값을 입력한다.
    	* transaction-manager 속성에는 스프링의 PlatformTransactionManager 빈을 설정한다.
    	* &lt;tx:method&gt; 태그는 &lt;tx:attributes&gt; 태그의 자식 태그로 설정, 트랜잭션 대상 메소드 및 트랜잭션 속성을 설정한다.
    	* &lt;tx:method&gt; 태그의 속성
    		* P523 표12.4
	* AOP를 통해 트랜잭션을 적용
		* P523 하단 설정
			* spring4 패키지의 하위 패키지에 있는 \*Service의 public 메소드에 Advisor를 적용하도록 설정
			* \*Service의 public 메소드 중에서 order 메소드를 호출할 때 트랜잭션이 적용된다.
***

###### (1) &lt;tx:method&gt; 태그의 rollback-for 속성과 no-rollback-for 속성을 통한 롤백 처리
* 스프링 트랜잭션은 기본적으로 RuntimeException 및 Error에 대해서만 롤백 처리 된다.
* Throwable이나 Exception 타입의 익셉션이 발생하더라도 롤백되지 않고 익셉션이 발생하기 전까지의 작업이 커밋된다.
* 익센셥 발생 시 트랜잭션의 롤백 규칙을 좀 더 정교하게 정의하려면
	* rollback-for
		* 익셉션 발생시 롤백 작업을 수행할 익셉션 타입을 설정
	* no-rollback-for
		* 익셉션 발생시 롤백하지 않을 익셉션 타입을 설정
	* 복수개일 경우는 콤마로 구분한다.

```XML
	<tx:method name="regist" rollback-for="Exception" no-rollback-for="MemberNotFoundException" />
```
> Exception 및 하위 타입의 익셉션이 발생할 경우 롤백 수행
> MemberNotFoundException이 발생할 경우 롤백 작업을 수행하지 않도록 설정

***

##### 5.2 어노테이션 기반 트랜잭션 설정
* @Transactional 어노테이션
	* 메소드나 클래스에 적용되며 관련 트랜잭션 속성을 설정한다.
	* P525 상단 예제
	* P525 표12.5 @Transactional 어노테이션의 주요 속성
	* 트랜잭션을 실제로 적용하려면
			<tx:annotation-driven transaction-manager="transactionManager" />
            ...
            <bean id="placeOrderService" class="....domain.PlaceOrderServiceAnnotImpl" />
            
	> &lt;tx:annotation-driven&gt; 태그를 설정해야 한다.
	> 자바코드 사용시는 @EnableTransactionManagement 어노테이션을 사용하면 된다 - P526 하단 설정

	* P526 표12.6 - &lt;tx:annotation-driven&gt; 태그의 속성
	* @EnableTransactionManagement 어노테이션과 &lt;tx:annotation-driven&gt; 태그의 차이점
		* &lt;tx:annotation-driven&gt; 태그는 사용할 PlatformTransactionManager 빈의 이름을 직접 지정
		* @EnableTransactionManagement 어노테이션은 PlatformTransactionManager 타입의 빈을 PlatformTransactionManager로 사용한다.
		* 직접 지정하려면 TransactionManagementConfigurer 인터페이스를 상속받은 자바 설정 코드를 추가해야 한다.
			* P527 하단 예제
		* P528 표12.7 @EnableTransactionManagement 어노테이션의 속성
***

###### (1) 트랜잭션 관리자 지정하기
* 한 개의 어플리케이션에서 두 개 이상의 DB를 사용할 때가 있다.
* 두 개의 DB에 동시에 변경을 가할 일이 없을때, 각 DB별로 트랜잭션 관리자를 따로 지정하는 것이 성능면에서 유리하다.
* DataSource별로 PlatformTransactionManager를 지정
	* P528 하단 설정
	> @Transactional 어노테이션을 만나면 memTxMgr이 관리하는 트랜잭션 범위 내에서 코드를 실행하게 된다.
	> orderTxMgr이 관리하는 트랜잭션 범위내에서 동작해야 하는 코드도 memTxMgr이 괸리하는 트랜잭션 내에서 동작하게 된다.

* @Transactional 어노테이션이 속할 트랜잭션 범위를 다르게 지정
	* value 속성값으로 사용할 PlatformTransactionManager 빈의 이름을 지정하면 된다.
	* 빈 등록 시 한정자를 (qualifier) 지정해도 된다.
	* P529 중간 예제/설정
***

##### 5.3 트랜잭션과 프록시
* 선언적 트랜잭션은 AOP를 사용한다.
* 트랜잭션을 처리하기 위해 빈 객체에 대한 프록시 객체를 생성한다.
* 이 프록시 객체는 PlatformTransactionManager를 이용해서 트랜잭션을 시작한 뒤, 실제 객체의 메서드를 호출하고 PTM을 이용해서 트랜잭션을 커밋한다.
* P529 그림12.4 프록시가 트랜잭션을 관리
* 선언적 트랜잭션은 AOP를 사용하므로 한 객체에 대해 복수개의 프록시 객체가 생성될수 있으며 순서가 중요하다면 명시적으로 지정해야 한다.
***

#### 6. TransactionsEssentials를 이용한 분산 트랜잭션
* 두 개 이상의 자원에 동시에 접근하는 트랜잭션
	* 결제와 구매 내역을 저장하는 DB가 다른 경우
	* 코드는 단일 트랜잭션으로 처리되어야 한다.
* 분산 트랜잭션
	* WebLogic, JBoss 등의 컨테이너는 자체적으로 지원
	* 톰캣 같은 서블릿 컨테이너는 지원하지 않음
	* 트랜잭션 매니저를 사용하면 된다.
		* TransactionEssentials나 Bitronix 등이 있음
* TransactionEssentials를 이용해 분산 트랜잭션을 설정해 보자.
***

##### 6.1 TransactionsEssentials 메이븐 설정
* TransactionsEssentials
	* Atomikos에서 개발한 오픈소스 버전
	* pom.xml에 등록만하면 사용 가능
		* P531 상단 메이븐 설정
***

###### 6.2 TransactionsEssentials와 스프링 연동
* 스프링 설정에 다음 정보 추가
	* TransactionsEssentials를 이용한 JtaTransactionManager 설정
	* TransactionsEssentials가 제공하는 클래스를 이용한 XADataSource 설정
	* DAO 등 스프링 빈에서 XADataSource를 사용하도록 설정
* TransactionsEssentials를 이용한 JtaTransactionManager 설정
	* P531 하단 설정 파일
* TransactionsEssentials를 이용한 XADataSource 설정
	* TransactionsEssentials는 다음의 두 가지 XADataSource를 제공
		* com.atomikos.jdbc.AtomikosDataSourceBean
			* XA를 지원하는 JDBC 드라이버를 위한 DataSource 설정
			* P533 상단 설정
				* uniqueResourceName
					* DataSource를 식별하는 고유 자원 이름
				* xaDataSourceClassName
					* XADataSource 구현 클래스의 완전한 이름
				* xaProperties
					* XADataSource를 설정할 때 필요한 <이름, 값> 쌍
		* com.atomikos.jdbc.nonxa.AtomikosNonXADataSourceBean
			* XA를 지원하지 않는 JDBC 드라이버를 위한 DataSource 설정
			* 트랜잭션의 원자성을 보장할 수 없다
			* P533 하단 설정
				* user, password, url, driverClassName 등의 프로퍼티를 가진다.
* TransactionsEssentials가 제공하는 XADataSource를 이용
	* 하나의 트랜잭션 범위에서 두 개 이상의 DataSource를 사용하도록 설정하자.
	* P534 중하단 코드
		* shopDataSource와 payDataSource에 접근하고 있음
	* placeOrderService
		* P535 중하단 소스
			* 트랜잭션 범위 내에서 두 개의 DataSource에 접근
			* TransactionsEssentials가 제공하는 글로벌 트랜잭션을 통해서 관리하기 때문에 하나의 트랜잭션으로 처리 됨
* 커넥션 풀 관련 프로퍼티
	* P536 표12.8 AtomikosDataSourceBean 클래스의 커넥션 풀 관련 설정
***


## Chapter16 스프링 시큐리티를 이용한 웹 보안

#### 1.웹 보안과 스프링 시큐리티
***

## Chapter 18 스프링 테스트 지원
* Junit4를 위한 지원 클래스를 제공
* 스프링 MVC 테스트를 위한 지원 클래스 제공
***

#### 01. 메이븐 의존 설정
* 스프링 제공 테스트 기능 사용 - 의존성 추가
	```XML
    	<dependency>
        	<groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>test</scope>
            <version>4.0.4.RELEASE</version>
        </dependency>
    	<dependency>
        	<groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>
    	<dependency>
        	<groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <version>1.3</version>
            <scope>test</scope>
        </dependency>
    ```
    > spring-test와 junit 모듈은 필수
    > hamcrest-library는 다양한 비교 기능 제공
***

#### 02. JUnit4의 스프링 테스트 통합 테스트
* 빈이 제대로 동작하는지 테스트하고 싶다면?
	* 다음과 같은 코드를 사용
		* P764 하단
		* 직접 컨테이너를 생성해서 특정 빈을 구하고, 이 빈 객체를 이용해서 테스트를 작성한다.
	* 스프링의 테스트 지원 기능을 사용한다면
		* P765 상단 코드
		* 컨테이너 생성 및 빈을 구하는 코드가 사라짐
		* @Autowired등이 사용 가능
***

##### 2.1 스프링 테스트 지원 기능 기본 사용
* 스프링 테스트 기능을 사용하려면
	* SpringJUnit4ClassRunner를 테스트 실행기로 지정
	* @ContextConfiguration 어노테이션으로 설정 정보 지정
	* 자동 주입 어노테이션을 이용해서 테스트에서 사용할 빈 객체 필드로 보관
	```JAVA
    import org.junit.runner.RunWith;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.test.context.ContextConfiguration;
    import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration("classpath:/springconfig.xml")
    // or @ContextConfiguration({"classpath:/springconfig.xml","spring-app"})  - 설정파일 목록을 배열로 사용
    // or @ContextConfiguration(location = {"classpath:/springconfig.xml","spring-app"}) - location 사용
    // or @ContextConfiguration(classes = SpringConf.class) -- 자바설정 사용 시
    // or @ContextConfiguration만 쓰면 "테스트클래스-context.xml" 파일을 설정 파일로 사용
    public class UseXmlConfTest {

        @Autowired
        private Calculator calculator;
    }
    ```
* SpringJunit4ClassRunner 클래스
	* JUnit의 Runner를 구현한 클래스
	* @ContextConfiguration에 지정한 설정 정보를 이용해 스프링 컨테이너를 생성
	* 빈 객체는 Autowired를 사용해서 필드로 선언하면 된다.
* @WebAppConfiguration 어노테이션
	* 스프링 MVC를 위한 설정을 이용해서 테스트 코드를 작성 가능
	* P767 중단 예제
	* 웹을 위한 WebApplicationContext 타입의 컨테이너를 생성
	* src/main/webapp 기준으로 자원을 구함
* 내부 중첩 클래스를 설정으로 사용
	```JAVA
    import org.junit.Test;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.test.context.ContextConfiguration;

    import javax.annotation.Resource;

    import static org.hamcrest.Matchers.equalTo;

    @ContextConfiguration
    public class UseInnerConfClassTest {
        @Resource(name = "calculator3")
        private Calculator calculator;

        @Test
        public void sum() {
            assertThat(calculator.sum(1, 2), equalTo(3L));
        }

        @Configuration
        public static class Config {
            @Bean
            public Calculator calculator3() {
                return new Calculator();
            }
        }
    }
    ```
    > 설정파일을 지정하지 않고 내부에 중첩클래스로 @Configuration 적용 - 설정 클래스로 사용됨
***

##### 2.2 설정 재사용
* 동일한 설정 코드를 사용하는 경우가 등장
	* P769 상단 코드
	* 스프링은 두가지 해결방안을 제시
		* 메타 어노테이션 사용
			* 어노테이션 설정을 묶어 놓은 또 다른 어노테이션을 사용
			```JAVA
            import org.springframework.test.context.ContextConfiguration;
            import org.springframework.test.context.web.WebAppConfiguration;

            import java.lang.annotation.ElementType;
            import java.lang.annotation.Retention;
            import java.lang.annotation.RetentionPolicy;
            import java.lang.annotation.Target;

            @ContextConfiguration({"classpath:/sringconfig.xml","classpath:/sring-mvc.xml"})
            @WebAppConfiguration
            @Retention(RetentionPolicy.RUNTIME)
            @Target(ElementType.TYPE)
            public @interface SpringTestConfig {
            }
            ```

			```JAVA
            import org.junit.runner.RunWith;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

            @RunWith(SpringJUnit4ClassRunner.class)
            @SpringTestConfig
            public class UseXmlConfTest {

                @Autowired
                private Calculator calculator;
            }
            ```
            > 메타 어노테이션 적용

		* 상속을 사용
			* 중복 설정을 담은 추상 클래스를 정의한 후 상속해서 사용
			* 추상 클래스 정의
			```JAVA
            import org.junit.runner.RunWith;
            import org.springframework.test.context.ContextConfiguration;
            import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
            import org.springframework.test.context.web.WebAppConfiguration;

            @RunWith(SpringJUnit4ClassRunner.class)
            @ContextConfiguration({"classpath:/sringconfig.xml","classpath:/sring-mvc.xml"})
            @WebAppConfiguration
            public abstract class AbstractCommonConfTest {
            }
            ```
			> .

			* 상속해서 사용

			```JAVA
            import org.junit.Test;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.context.ApplicationContext;

            public class ReuseParentConfTest extends AbstractCommonConfTest {
                @Autowired
                private ApplicationContext context;

                @Test
                public void beanExists() {
                    assertThat(context.containsBean("calculator"), equals(true));
                }
            }
            ```
            > 추가 설정 필요 시 @ContextConfiguration 어노테이션으로 추가 가능

			* 상위 클래스 설정 사용하지 않음
			```JAVA
            import org.junit.Test;
            import org.springframework.beans.factory.annotation.Autowired;
            import org.springframework.context.ApplicationContext;
            import org.springframework.test.context.ContextConfiguration;

            @ContextConfiguration(value="classpath:/springconf2.xml", inheritLocations = false)
            public class ReuseParentConfTest extends AbstractCommonConfTest {
                @Autowired
                private ApplicationContext context;

                @Test
                public void beanExists() {
                    assertThat(context.containsBean("calculator"), equals(true));
                }
            }
            ```
            > inheritLocation = false로 설정
***

##### 2.3 프로필 선택
* 복수개 프로필이 있을 경우 @ActiveProfiles로 선택할수 있다.
* P771 하단
***

##### 2.4 컨텍스트 리로딩 처리
* JUnit은 각 테스트 메서드를 실행할 때마다 테스트 클래스의 객체를 생성한다.
* 다음 테스트의 경우 스프링 컨텍스트는 몇 개 생성될까?
    ```JAVA
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

        import static org.hamcrest.CoreMatchers.equalTo;

        @RunWith(SpringJUnit4ClassRunner.class)
        @SpringTestConfig
        public class UseXmlConfTest {

            @Autowired
            private Calculator calculator;

            @Test
            public void sum() {
                assertThat(calculator.sum(1, 2), equalTo(3L));
            }

            @Test
            public void sum1() {
                assertThat(calculator.sum(3, 4), equalTo(7L));
            }

            @Test
            public void sum2() {
                assertThat(calculator.sum(5, 6), equalTo(11L));
            }
        }
    ```
    
    *
    *
    *
    *
    * SpringJUnit4ClassRunner는 스프링 컨텍스트를 한번만 생성하고 각 테스트 메서드마다 스프링 컨텍스트를 재사용한다.
    * @DirtiesContext
    	* 메서드 실행 후 스프링 컨텍스트를 초기화한다.
    	* 클래스에도 적용할수 있으며, 이때는 매 메소드 실행시마다 초기화 한다.
    		`@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD) 사용`
            | classMode | 의미 |
			|--------|--------|
            | AFTER_EACH_TEST_METHOD | 각 테스트 메소드 실행 후 컨텍스트를 초기화 |
			| AFTER_CLASS       | 테스트클래스의 모든 테스트 메소드 실행을 마친 후에 컨텍스트를 리로딩 |
***

##### 2.5 테스트 코드의 트랜잭션 처리 지정
* DB 연동을 포함한 테스트 수행 시, 결과로 DB값이 변경되어 이후 테스트가 실패하게 된다.
	* P775 예제
* 해결방법
	* 스프링의 트랜잭션 롤백 기능 사용
	* @TransactionConfiguration 어노테이션과 @Transactional 어노테이션 사용
	* P776 예제
	* @TransactionConfiguration 어노테이션
		* PlatformTransactionManager를 이용해 트랜잭션 처리
		* P507
		* 이름이 transactionManager인 빈을 트랜잭션 관리자로 사용
		* transactionManager인 빈이 없다면 별도로 지정해야 함 - P777 상단
        * 특정 메소드에 대해서만 트랜잭션을 롤백하고 싶다면
        ```JAVA
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.springframework.test.annotation.Rollback;
        import org.springframework.test.context.ContextConfiguration;
        import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
        import org.springframework.test.context.transaction.TransactionConfiguration;

        import static org.hamcrest.MatcherAssert.assertThat;
        import static org.hamcrest.number.OrderingComparison.greaterThan;

        @RunWith(SpringJUnit4ClassRunner.class)
        @ContextConfiguration({"classpath:/applicationContext.xml"})
        @TransactionConfiguration(defaultRollback = false)
        @Transactional
        public class MessageDaoIntTest {
            @Rollback(true)
            @Test
            public void insert() {
                Message message = new Message();
                message.setName("bkchoi");
                int newMessageId = messageDao.insert(message);
                assertThat(newMessageId, greaterThan(0));
            }
        }
        ```
        > But!
        > TransactionConfiguration은 deprecated 됨
        > https://examples.javacodegeeks.com/enterprise-java/spring/write-transactional-unit-tests-spring/

* DB의 경우는 DBUnit 사용을 권장 - 저자
***

#### 03 스프링 MVC 테스트
* 컨트롤러 검증을 위해 MockMVC 제공
	* 지정된 경로로 리다이렉트 처리 여부
	* JSON 응답을 올바르게 응답하는지 여부
***

##### 3.1 스프링 테스트의 MockMvc 사용하기
* 스프링 MVC의 테스트 코드
	```JAVA
    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration("classpath:/spring-mvc.xml")
    // @ContextConfiguration(classes = WebConfig.class)
    @WebAppConfiguration
    public class HomeControllerTest {
        @Autowired
        private WebApplicationContext ctx;

        private MockMvc mockMvc;

        @Before
        public void setUp(){
            mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
        }

        @Test
        public void testHello() throws Exception {
            mockMvc.perform(get("/hello").param("name", "bkchoi"))
                    .andExpect(status().isOk())
                    .andExpect(model().attributeExists("greeting"));
        }
    }
    ```
	> perform() - HttpServletRequest를 생성하고 DispatcherServlet을 실행한다.
	> andExpect() - 컨트롤러가 생성한 응답을 확인한다.

* 관련 컨트롤러와 설정 파일 - P780
***

##### 3.2 MockMvc 생성 방법
* 다음 두가지 방법으로 생성 가능
	* WebApplicationContext를 이용해서 생성
        * P781 하단 예제
        * 완전한 스프링 MVC 환경을 이용해서 테스트 가능
	* 테스트하려는 컨트롤러를 이용해서 생성
		* P782 하단 예제
		* MockMvcBuilders.standaloneSetup() 메서드에 컨트롤러 객체를 전달받아 MockMvc 객체를 생성
		* 전달받은 컨트롤러만 테스트 대상으로 사용
		* 필터 적용시는 addFilter() 사용 - P783 중단 예제
***

##### 3.3 요청 구성
* 객체 생성 후에 perform() 메서드를 이용해서 RequestBuilder타입 인자를 받은 후 요청을 DispatcherServlet에 전송
* MockMvcRequestBuilders클래스를 사용해서  RequestBuilder를 생성
	* MockMvcRequestBuilders의 주요 정적 메서드
		* P784 상단
		* 정적 메소드를 정적 임포트를 사용해 가독성을 높인다.
* P784 요청구성 예제
* 요청 정보 생성 관련 주요 메서드 - P785 표 18.1
* 요청 정보는 메서드 체인 방식으로 구성 가능
* o.s.http.MediaType 클래스에 정의된 주요 상수 값
	* APPLICATION\_FORM_URLENCODED (application/x-www-form-urlencoded)
	* MULTIPART\_FORM\_DATA (multipart/form-data)
	* APPLICATION\_OCTET\_STREAM (application/octet-stream)
	* APPLICATION\_JSON (application/json)
	* APPLICATION\_XML (application/xml)
	* TEXT\_XML (text/xml)
	* TEXT\_HTML (text/html)
	* TEXT\_PLAIN (text/plain)
***

##### 3.4 응답 검증
* ResultActions을 리턴한다.
	* andExpect(ResultMatcher matcher) 메소드로 결과를 검증한다.
	* P786 예제
* status(), joinPath() 등 다양한 정적 메소드가 존재한다.
***

###### (1) 상태 코드 검증
* 응답 결과를 검증하기 위해 사용되는 정적 메소드
	* status()
		* StatusResultMatchers 객체를 리턴
		* 응답 상태 코드 확인을 위해 제공하는 메소드 - P787 표18.2
***

###### (2) 뷰/리다이렉트 이름 검증
* MockMvcResultMatchers.view() 메소드를 사용한다.
* P787 하단 예제
* redirectUrl()
	* 리다이렉트 응답(경로)을 검사
***

###### (3) 모델 확인
* 컨트롤러에서 생성한 모델을 검사
* MockMvcresultMatchers.model() 메소드 사용
	* P788 중단 예제
* 모델값을 검증하기 위해 제공하는 메소드 - P788 표18.3
* 사용 예
	* P789 상단 예
***

###### (4) 헤더 검증
* MockMvcresultMatchers.header() 메소드를 사용
* P789 중단 예
* 헤더값을 검증하기 위해 제공하는 메소드 - P789 표18.4
***

###### (5) 쿠키 검증
* 응답 결과로 생성되는 쿠키를 확인
* MockMvcresultMatchers.cookie() 메소드를 사용
* P789 하단 예
* 쿠키값을 검증하기 위해 제공하는 메소드 - P790 표18.5
***

###### (6) JSON 응답 검증
* 컨트롤러가 JSON 응답을 리턴할때 검증하기 위해 MockMvcresultMatchers.jsonPath() 메소드를 사용
* json-path 디펜던시를 추가해야 함
	```XML
    <dependency>
    	<groupId>com.jayway.jsonpath</groupId>
        <artifactId>json-path</artifactId>
        <version>0.9.0</version>
        <scope>test</scope>
    </dependency>
    ```
* P790 하단 예
* JSON 응답을 검증하기 위해 제공하는 메소드 - P791 표18.6
***

###### (7) XML 응답 검증
* MockMvcresultMatchers.xpath() 메소드를 사용
* xpath 경로를 사용해서 XML 응답을 검사할 수 있다.
	```JAVA
   	mockMvc.perform(get("/books.xml"))
       	.andExpect(xpath("/book-list/book[3]/title").string("제목3"))
        .andExpect(xpath("/book-list/book[3]/%s", "price").number(3000.0));
    ```
* 값 검증을 위해 제공하는 메소드 - P792 표18.7
***

###### (8) 요청/응답 내용 출력
* MockMvc가 생성하는 요청과 응답 내용을 출력하기 위한 방법
* P793 상단 코드
	* andDo와 print() 사용
* 콘솔 출력 내용을 통해 문제점 파악에 사용 가능하다.
***

