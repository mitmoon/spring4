package sff.spring4;

import org.springframework.beans.factory.BeanNameAware;

/**
 * Created by mitmoon on 2017-11-05.
 */
public class WorkRunner implements BeanNameAware {
    private String beanId;

    public void setBeanName(String name) {
        this.beanId = name;
    }

    public void execute(Work work) {
        System.out.printf("WorkRunner[%s] execute Work[%d]\n", beanId, work.getOrder());
    }
}
