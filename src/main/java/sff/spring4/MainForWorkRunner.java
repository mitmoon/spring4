package sff.spring4;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by mitmoon on 2017-11-05.
 */
public class MainForWorkRunner {
    public static void main(String[] args){
        useXml();
    }

    private static void useXml() {
        System.out.println("===== XML Meta =====");
        String configLocation = "classpath:config-for-scope.xml";
        GenericXmlApplicationContext ctx =
                new GenericXmlApplicationContext(configLocation);

        WorkScheduler scheduler = ctx.getBean("workScheduler", WorkScheduler.class);
        scheduler.makeAndRunWork();

        ctx.close();
    }
}
