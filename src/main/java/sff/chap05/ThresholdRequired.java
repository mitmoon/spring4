package sff.chap05;

/**
 * Created by mitmoon on 2017-11-12.
 */
public interface ThresholdRequired {
    public void setThreshold(int threshold);
}
