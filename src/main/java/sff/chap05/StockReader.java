package sff.chap05;

import java.util.Date;

/**
 * Created by mitmoon on 2017-11-12.
 */
public interface StockReader {
    public int getClosePrice(Date date, String code);
}
