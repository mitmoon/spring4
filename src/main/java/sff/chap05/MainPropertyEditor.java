package sff.chap05;

import org.springframework.context.support.GenericXmlApplicationContext;

public class MainPropertyEditor {

	public static void main(String[] args) {
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:propertyEditor.xml");
		RestClient restClient = ctx.getBean(RestClient.class);
		System.out.println(restClient.toString());
		ctx.close();

/*		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:propertyEditor.xml");
		InvestmentSimulator investmentSimulator = ctx.getBean(InvestmentSimulator.class);

		System.out.println(investmentSimulator.getMinimumAmount());
		ctx.close();*/
	}

}
