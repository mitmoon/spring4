package sff.chap05;

/**
 * Created by mitmoon on 2017-11-12.
 */
public class DataCollector implements ThresholdRequired {
    private int threshold;

    @Override
    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getThreshold() {
        return threshold;
    }
}
