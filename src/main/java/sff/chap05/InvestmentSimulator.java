package sff.chap05;

public class InvestmentSimulator {

	private Money minimumAmount;

	public void setMinimumAmount(Money minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	
	public void simulate(Money amountInvested) {
		// TODO
	}

	public Money getMinimumAmount() {
		return minimumAmount;
	}
}
