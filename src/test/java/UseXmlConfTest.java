import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by mitmo on 2018-01-15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringTestConfig
public class UseXmlConfTest {

    @Autowired
    private Calculator calculator;

    @Test
    public void sum() {
        assertThat(calculator.sum(1, 2), equalTo(3L));
    }

    @Test
    public void sum1() {
        assertThat(calculator.sum(3, 4), equalTo(7L));
    }

    @Test
    public void sum2() {
        assertThat(calculator.sum(5, 6), equalTo(11L));
    }
}
