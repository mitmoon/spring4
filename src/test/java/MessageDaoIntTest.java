import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;

/**
 * Created by mitmo on 2018-01-16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext.xml"})
@TransactionConfiguration(defaultRollback = false)
@Transactional
public class MessageDaoIntTest {
    @Rollback(true)
    @Test
    public void insert() {
        Message message = new Message();
        message.setName("bkchoi");
        int newMessageId = messageDao.insert(message);
        assertThat(newMessageId, greaterThan(0));
    }
}
