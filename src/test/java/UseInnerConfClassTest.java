import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.equalTo;

/**
 * Created by mitmo on 2018-01-15.
 */
@ContextConfiguration
public class UseInnerConfClassTest {
    @Resource(name = "calculator3")
    private Calculator calculator;

    @Test
    public void sum() {
        assertThat(calculator.sum(1, 2), equalTo(3L));
    }

    @Configuration
    public static class Config {
        @Bean
        public Calculator calculator3() {
            return new Calculator();
        }
    }
}
