import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by mitmo on 2018-01-15.
 */
@ContextConfiguration(value="classpath:/springconf2.xml", inheritLocations = false)
public class ReuseParentConfTest extends AbstractCommonConfTest {
    @Autowired
    private ApplicationContext context;

    @Test
    public void beanExists() {
        assertThat(context.containsBean("calculator"), equals(true));
    }
}
